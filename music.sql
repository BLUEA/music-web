/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : music

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 31/12/2020 18:56:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_UNIQUE`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (3, 'admin', '123');

-- ----------------------------
-- Table structure for child_comment
-- ----------------------------
DROP TABLE IF EXISTS `child_comment`;
CREATE TABLE `child_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '子评论的id，唯一识别',
  `father_id` int(10) NULL DEFAULT NULL COMMENT '父评论的id，不做唯一识别',
  `from` int(10) NULL DEFAULT NULL COMMENT '回复评论的用户ID',
  `to` int(10) NULL DEFAULT NULL COMMENT '被回复的对象用户id',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回复内容',
  `up` int(10) NULL DEFAULT NULL COMMENT '点赞数',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of child_comment
-- ----------------------------
INSERT INTO `child_comment` VALUES (2, 59, 29, 29, '小丑竟在我身边', NULL, '2020-12-28 16:30:10');
INSERT INTO `child_comment` VALUES (3, 58, 29, 29, '111', NULL, '2020-12-29 03:30:10');

-- ----------------------------
-- Table structure for collect
-- ----------------------------
DROP TABLE IF EXISTS `collect`;
CREATE TABLE `collect`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `song_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `song_list_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of collect
-- ----------------------------
INSERT INTO `collect` VALUES (52, 29, 0, 119, NULL, '2020-12-28 14:01:30');
INSERT INTO `collect` VALUES (53, 29, 0, 116, NULL, '2020-12-28 16:28:05');
INSERT INTO `collect` VALUES (55, 29, 0, 114, NULL, '2020-12-29 08:59:40');
INSERT INTO `collect` VALUES (59, 29, 0, 113, NULL, '2020-12-29 09:00:25');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `song_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `song_list_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `up` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (58, 29, 113, NULL, '好好听', '2020-12-28 14:02:23', 0, 0);
INSERT INTO `comment` VALUES (59, 29, 116, NULL, '太棒了', '2020-12-28 16:29:35', 0, 0);
INSERT INTO `comment` VALUES (61, 29, 113, NULL, 'bucuo', '2020-12-29 03:30:04', 0, 0);
INSERT INTO `comment` VALUES (62, 29, 114, NULL, '不错', '2020-12-29 08:59:17', 0, 0);
INSERT INTO `comment` VALUES (63, 29, NULL, 85, '不错，真不错', '2020-12-30 14:20:40', 1, 0);

-- ----------------------------
-- Table structure for consumer
-- ----------------------------
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL,
  `phone_num` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` datetime(0) NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username_UNIQUE`(`username`) USING BTREE,
  UNIQUE INDEX `phone_num_UNIQUE`(`phone_num`) USING BTREE,
  UNIQUE INDEX `email_UNIQUE`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of consumer
-- ----------------------------
INSERT INTO `consumer` VALUES (29, 'bluea', '123456', 1, '15217769063', '1654058607@qq.com', '2020-11-30 16:00:00', '听一首歌，做一件事000', '广东', '/avatarImages/1609212744633109951165177312849.jpg', '2020-12-28 09:18:38', '2020-12-29 03:32:32');

-- ----------------------------
-- Table structure for list_song
-- ----------------------------
DROP TABLE IF EXISTS `list_song`;
CREATE TABLE `list_song`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `song_id` int(10) UNSIGNED NOT NULL,
  `song_list_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 212 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of list_song
-- ----------------------------
INSERT INTO `list_song` VALUES (209, 116, 85);
INSERT INTO `list_song` VALUES (210, 116, 84);
INSERT INTO `list_song` VALUES (211, 113, 84);

-- ----------------------------
-- Table structure for singer
-- ----------------------------
DROP TABLE IF EXISTS `singer`;
CREATE TABLE `singer`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(4) NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `birth` datetime(0) NULL DEFAULT NULL,
  `location` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of singer
-- ----------------------------
INSERT INTO `singer` VALUES (43, '周柏豪', 1, '/img/singerPic/1609161007100周柏豪.jfif', '1984-11-21 16:00:00', '香港', '周柏豪幼时居住在青衣长康邨[4]。他在1996年毕业于圣公会仁立小学（下午校），后来搬到将军澳颂明苑居住，并入读东华三院吕润财纪念中学，至2001年毕业。');
INSERT INTO `singer` VALUES (44, '洪卓立', 1, '/img/singerPic/1609162055290洪卓立.jfif', '1987-03-01 16:00:00', '香港', '香港男歌手。于2006年参加无线电视与英皇娱乐合办的“2006年度英皇新秀歌唱大赛”夺得亚军而晋身乐坛，成为英皇娱乐旗下歌手。他于2019年8月18日开人生第一场香港个人演唱会，名为《回到最爱的那天演唱会》，但因社会活动而被迫取消');
INSERT INTO `singer` VALUES (45, '范玮琪', 0, '/img/singerPic/1609162584083范玮琪.jfif', '1976-03-17 16:00:00', '美国俄亥俄州', '范玮琪（Fan Fan），1976年3月18日出生于美国俄亥俄州，华语流行乐女歌手、影视演员、主持人。');
INSERT INTO `singer` VALUES (46, '林俊杰', 1, '/img/singerPic/1609170745698林俊杰.jfif', '1981-03-26 16:00:00', '中国福建', '林俊杰（JJ Lin），1981年3月27日出生于新加坡，祖籍中国福建省厦门市同安区 [1]  ，华语流行乐男歌手、作曲人、音乐制作人、潮牌主理人。');
INSERT INTO `singer` VALUES (47, '周杰伦', 1, '/img/singerPic/1609163783727周杰伦.jfif', '1979-01-17 16:00:00', '中国台湾', '周杰伦（Jay Chou），1979年1月18日出生于台湾省新北市，祖籍福建省泉州市永春县，中国台湾流行乐男歌手、原创音乐人、演员、导演、编剧，毕业于淡江中学。');
INSERT INTO `singer` VALUES (48, '陈奕迅', 1, '/img/singerPic/1609169364922陈奕迅.jfif', '1974-07-26 16:00:00', '广东省东莞市', '陈奕迅（Eason Chan），1974年7月27日出生于中国香港，祖籍广东省东莞市，华语流行乐男歌手、演员、作曲人，毕业于英国金斯顿大学。');
INSERT INTO `singer` VALUES (49, '李克勤', 1, '/img/singerPic/1609170220292李克勤.jfif', '1967-12-05 16:00:00', '中国香港', '李克勤（Hacken Lee），1967年12月6日生于中国香港，籍贯广东新会崖西，中国香港男歌手、演员、主持人。\n1985年，凭《雾之恋》夺得十九区业余歌唱大赛冠军进入乐坛。 1986年，推出首张同名EP《李克勤》。1993年，转投星光唱片公司，并凭借歌曲《回首》获得1993年度十大劲歌金曲的金曲奖。1998年夏天，李克勤为TVB转播之世界杯足球赛事担任主持。');
INSERT INTO `singer` VALUES (50, '张杰', 1, '/img/singerPic/1609170942243张杰.jfif', '1982-12-19 16:00:00', '四川成都', '张杰（Jason Zhang），1982年12月20日出生于四川省成都市，中国流行男歌手');
INSERT INTO `singer` VALUES (51, '郭静', 0, '/img/singerPic/1609170762890郭静.jfif', '1990-08-04 15:00:00', '香港', '郭静（Claire），本名郭伯瑜 ，1980年8月5日出生于香港，中国台湾流行乐女歌手、主持人，前“Monster”竞技啦啦队的队员，毕业于世新大学图文传播暨数位出版学系');
INSERT INTO `singer` VALUES (52, '毛不易', 1, '/img/singerPic/1609170856819毛不易.jfif', '1994-09-30 16:00:00', '黑龙江省齐齐哈尔市泰来县', '毛不易，原名王维家，1994年10月1日出生于黑龙江省齐齐哈尔市泰来县，中国内地流行乐男歌手，毕业于杭州师范大学护理专业。');
INSERT INTO `singer` VALUES (53, 'Aimer', 0, '/img/singerPic/1609171382187Aimer.jfif', '1991-07-08 15:00:00', '日本', 'Aimer，日本女歌手，在2011年9月7日以单曲「六等星の夜/悲しみはオーロラに/TWINKLE TWINKLE LITTLE STAR」正式出道，隶属唱片公司是日本索尼音乐娱乐旗下的DefSTAR Records，2015年9月移籍到SME Records。');

-- ----------------------------
-- Table structure for song
-- ----------------------------
DROP TABLE IF EXISTS `song`;
CREATE TABLE `song`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `singer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NOT NULL COMMENT '发行时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `lyric` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 122 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of song
-- ----------------------------
INSERT INTO `song` VALUES (113, 43, '周柏豪-宏愿', 'Continue', '2020-12-28 13:12:54', '2020-12-28 13:12:54', '/img/songPic/1609161425240宏愿.jfif', '[00:00.44]周柏豪 - 宏愿\r\n[00:01.44]作词：林夕\r\n[00:02.29]作曲：KAY CHAN\r\n[00:14.40]明知残酷亦只可以面对\r\n[00:20.51]期望你回望我\r\n[00:23.26]无疑更受罪\r\n[00:26.26]礼物无知\r\n[00:28.11]还在走的挂钟\r\n[00:30.06]都似挂住心碎\r\n[00:32.65]还提示你与我\r\n[00:34.35]无限分钟的过去\r\n[00:39.14]明知怀旧换不到勇气\r\n[00:44.99]然而若要放下你\r\n[00:48.08]如行过赤地\r\n[00:50.83]何妨天真\r\n[00:52.56]想想总有日可在一起\r\n[00:57.26]离别几千天当做嬉戏\r\n[01:03.02]如果等到的只得幽怨\r\n[01:06.10]也是我心愿\r\n[01:08.90]如果可将光阴都扭转\r\n[01:12.10]我未怕酸\r\n[01:15.35]如果可分享当初恩怨\r\n[01:18.40]也未怕又一次失恋\r\n[01:21.89]宏愿是大到这样细\r\n[01:24.29]再与你入戏院\r\n[01:27.47]流星假使比哭声心软\r\n[01:30.37]我笑着去许愿\r\n[01:33.52]离开假使只因性格\r\n[01:36.42]性格任你选\r\n[01:39.67]奇迹假使一生得一次\r\n[01:43.07]这晚让我抱回这份暖\r\n[01:48.97]遥遥望见你回头怎算是远\r\n[01:55.67]如宏愿太过渺茫不去预算\r\n[02:26.86]还好仍然未舍得放弃\r\n[02:32.73]才明白我会为爱和时间竞技\r\n[02:38.53]纯如清水只想跟你又走到一起\r\n[02:44.89]承受些伤悲也是福气\r\n[02:50.46]如果等到的只得幽怨\r\n[02:53.94]也是我心愿\r\n[02:56.76]如果可将光阴都扭转\r\n[02:59.71]我未怕酸\r\n[03:02.91]如果可分享当初恩怨\r\n[03:06.01]也未怕又一次失恋\r\n[03:09.51]宏愿是大到这样细\r\n[03:11.91]再与你入戏院\r\n[03:15.06]流星假使比哭声心软\r\n[03:18.36]我笑着去许愿\r\n[03:21.31]离开假使只因性格\r\n[03:24.16]性格任你选\r\n[03:27.36]奇迹假使一生得一次\r\n[03:30.76]这晚让我抱回这份暖\r\n[03:37.54]幻想比心死更热暖\r\n[03:43.63]未管新的衫比昨日暖', '/song/周柏豪 - 宏愿.mp3');
INSERT INTO `song` VALUES (114, 43, '周柏豪-传闻', '传闻', '2020-12-28 13:20:58', '2020-12-28 13:20:58', '/img/songPic/1609161780334传闻.jpg', '[00:00.56]周柏豪 - 传闻\r\n[00:01.73]作词：陈咏谦\r\n[00:02.85]作曲：周柏豪\r\n[00:21.85]这些年来\r\n[00:23.94]曾经多次跌落地牢\r\n[00:26.93]仍然等待\r\n[00:31.73]这些年来\r\n[00:33.67]和抑郁与狂躁对话\r\n[00:37.03]依然忍耐\r\n[00:41.05]为了要抱你抱到最后\r\n[00:45.88]用我的体恤体谅补救\r\n[00:51.31]得到是你无情冷笑\r\n[00:55.18]松开我手\r\n[01:00.35]知不知你在滥用我的恻隐\r\n[01:05.74]当我愈来愈沉迷像**\r\n[01:10.87]外界很多传闻\r\n[01:14.13]说你有数段孽缘藏在我附近\r\n[01:20.69]追踪只恐怕侮辱我的身份\r\n[01:25.98]蒙着耳朵\r\n[01:27.50]双手不准抖震\r\n[01:30.76]很快便忘记了\r\n[01:33.91]难堪都不要紧\r\n[01:45.97]但你要计较每个错漏\r\n[01:51.31]像挖空心思也不足够\r\n[01:56.13]何不回头\r\n[01:58.88]回到当天放你走\r\n[02:05.49]知不知你在滥用我的恻隐\r\n[02:10.72]当我愈来愈沉迷像**\r\n[02:15.85]外界很多传闻\r\n[02:19.11]说你有数段孽缘藏在我附近\r\n[02:25.51]追踪只恐怕侮辱我的身份\r\n[02:30.85]蒙着耳朵\r\n[02:32.83]双手不准抖震\r\n[02:35.73]很快便忘记了\r\n[02:38.98]忘记这段人生\r\n[03:00.63]知不知我共你原著多缤纷\r\n[03:05.55]相信共你的遗传极合衬\r\n[03:10.54]但你总不断恨\r\n[03:13.89]要继续捏造罪名蚕食这幸运\r\n[03:20.50]终于都知道我能有多低等\r\n[03:25.84]缠住你手\r\n[03:27.57]走不开拉不近\r\n[03:30.67]只要仍然爱你\r\n[03:33.93]其它都不要紧\r\n[03:48.36]请你回头细看\r\n[03:51.96]欣赏我这牧人', '/song/周柏豪 - 传闻.mp3');
INSERT INTO `song` VALUES (115, 44, '洪卓立-心淡', '2014麦王争霸第四季第四期现场', '2020-12-28 13:29:36', '2020-12-28 13:30:22', '/img/songPic/tubiao.jpg', '[00:00:00]暂无歌词', '/song/洪卓立 - 心淡 - 2014麦王争霸第四季第四期现场.mp3');
INSERT INTO `song` VALUES (116, 45, '范玮琪-灰色的彩虹', 'F One', '2020-12-28 13:37:44', '2020-12-28 13:37:44', '/img/songPic/1609162765715灰色的彩虹.jpg', '[00:04.75]范玮琪 - 灰色的彩虹\r\n[00:13.58]词：王雅君\r\n[00:14.58]曲：范玮琪\r\n[00:19.10]我从秋天等到安静的落叶\r\n[00:23.96]还不够时间倒带想念\r\n[00:28.82]就像电影情节最后完结篇\r\n[00:33.66]褪色的画面没有想念\r\n[00:39.17]我的记忆摇晃着昨天\r\n[00:44.18]我还有感觉\r\n[00:49.03]一抬头什么都看不见\r\n[00:54.03]雨后的屋檐\r\n[01:00.79]红橙黄绿我都找不到的晴天\r\n[01:06.20]从此我们两个世界\r\n[01:10.07]在灰色季节渐渐忘记你的一切\r\n[01:15.93]过几年我在原点\r\n[01:20.34]彩虹出现而我再也找不到美丽的蝴蝶\r\n[01:28.00]偏偏飞不上天\r\n[01:29.86]对你的想念\r\n[01:32.00]再也寄不到你世界\r\n[01:35.42]地址是再见\r\n[01:52.01]我从秋天等到安静的落叶\r\n[01:56.88]还不够时间倒带想念\r\n[02:01.97]就像电影情节最后完结篇\r\n[02:06.72]褪色的画面没有想念\r\n[02:12.24]我的记忆摇晃着昨天\r\n[02:17.05]我还有感觉\r\n[02:21.95]一抬头什么都看不见\r\n[02:26.88]雨后的屋檐\r\n[02:33.67]红橙黄绿我都找不到的晴天\r\n[02:39.12]从此我们两个世界\r\n[02:43.20]在灰色季节渐渐忘记你的一切\r\n[02:48.98]过几年我在原点\r\n[02:53.31]彩虹出现而我再也找不到美丽的蝴蝶\r\n[03:00.98]偏偏飞不上天\r\n[03:02.80]对你的想念\r\n[03:04.93]再也寄不到你世界\r\n[03:08.60]地址是再见\r\n[03:30.07]红橙黄绿我都找不到的晴天\r\n[03:35.51]从此我们两个世界\r\n[03:39.48]在灰色季节渐渐忘记你的一切\r\n[03:45.32]过几年我在原点\r\n[03:49.64]彩虹出现而我再也找不到美丽的蝴蝶\r\n[03:57.33]偏偏飞不上天\r\n[03:59.07]对你的想念\r\n[04:02.40]寄不到你世界\r\n[04:04.78]地址是再见', '/song/灰色的彩虹.mp3');
INSERT INTO `song` VALUES (117, 46, '林俊杰-学不会', '学不会', '2020-12-28 13:46:46', '2020-12-28 13:46:46', '/img/songPic/1609163258111学不会.jfif', '[00:00.000] 作曲 : 林俊杰\r\n[00:01.000] 作词 : 姚若龙\r\n[00:16.43]你的痛苦我都心疼\r\n[00:18.47]想为你解决\r\n[00:23.85]挡开流言 紧握你手\r\n[00:26.24]想飞奔往前\r\n[00:31.04]我相信爱 能证明一切\r\n[00:34.67]够真心 会超越时间\r\n[00:38.62]多付出 也多了喜悦\r\n[00:42.55]让幸福蔓延\r\n[00:48.45]总是学不会 再聪明一点\r\n[00:56.08]记得自我保护 必要时候讲些\r\n[01:00.72]善意谎言\r\n[01:03.61]总是学不会\r\n[01:07.56]真爱也有现实面\r\n[01:13.44]不是谁情愿\r\n[01:17.43]就能够解决\r\n[01:30.14]一次争吵 一个心结\r\n[01:31.99]累积着改变\r\n[01:37.78]内心疏远 足够秒杀\r\n[01:39.69]外表多浓烈\r\n[01:44.42]才发现爱 不代表一切\r\n[01:48.22]再真心 也会被阻绝\r\n[01:52.05]这世界 天天有诡雷\r\n[01:55.69]随时会爆裂\r\n[02:00.08]还是学不会 少浪漫一点\r\n[02:07.62]拼命着想的事 未必带来感动\r\n[02:12.41]或被感谢\r\n[02:15.42]还是学不会\r\n[02:19.26]解释我最伤 最累\r\n[02:26.55]痛死都不愿 怪谁\r\n[02:35.62]把每段痴情苦恋\r\n[02:39.31]在此刻排列面前\r\n[02:43.22]也感觉不埋怨 只怀念\r\n[02:55.30]总是学不会 再聪明一点\r\n[03:02.70]记得自我保护 必要时候讲些\r\n[03:07.88]善意谎言\r\n[03:10.67]不是学不会\r\n[03:14.56]只是觉得 爱太美\r\n[03:22.26]值得去沉醉\r\n[03:29.94]流泪', '/song/林俊杰-学不会.mp3');
INSERT INTO `song` VALUES (118, 46, '林俊杰-期待爱', 'JJ陆', '2020-12-28 13:50:28', '2020-12-28 13:50:28', '/img/songPic/1609163613410期待爱.jpg', '[00:00.46]林俊杰、金莎 - 期待爱\r\n[00:02.14]作词：林怡凤、许环良\r\n[00:03.90]作曲：林俊杰\r\n[00:33.87]My life\r\n[00:35.28]一直在等待\r\n[00:38.08]空荡的口袋\r\n[00:40.86]想在里面放一份爱\r\n[00:45.22]Why总是被打败\r\n[00:49.33]真的好无奈\r\n[00:53.03]其实我实实在在\r\n[00:55.26]不管帅不帅\r\n[00:59.37]想要找回来\r\n[01:02.05]自己的节拍\r\n[01:04.89]所以这一次\r\n[01:07.79]我要勇敢\r\n[01:09.33]大声说出来\r\n[01:13.03]期待\r\n[01:14.55]期待你发现我的爱\r\n[01:17.95]无所不在\r\n[01:20.18]我自然而然的关怀\r\n[01:23.82]你的存在\r\n[01:26.53]心灵感应的方向\r\n[01:28.91]我一眼就看出来\r\n[01:32.71]是因为爱\r\n[01:35.56]我猜\r\n[01:37.02]你早已发现我的爱\r\n[01:40.62]绕几个弯\r\n[01:42.75]靠越近越明白\r\n[01:46.39]不要走开\r\n[01:49.21]幸福的开始\r\n[01:50.80]就是放手去爱\r\n[02:21.29]想要找回来\r\n[02:23.98]自己的节拍\r\n[02:26.82]所以这一次\r\n[02:29.67]我要勇敢\r\n[02:31.04]大声说出来\r\n[02:34.93]期待\r\n[02:36.45]期待你发现我的爱\r\n[02:39.89]无所不在\r\n[02:42.07]我自然而然的关怀\r\n[02:45.61]你的存在\r\n[02:48.46]心灵感应的方向\r\n[02:50.74]我一眼就看出来\r\n[02:54.59]是因为爱\r\n[02:57.59]我猜\r\n[02:59.00]你早已发现我的爱\r\n[03:02.45]绕几个弯\r\n[03:04.62]靠越近越明白\r\n[03:08.22]不要走开\r\n[03:10.91]幸福的开始\r\n[03:12.64]就是放手去爱\r\n[03:19.51]幸福的开始\r\n[03:21.41]就是放手去爱', '/song/林俊杰-期待爱.mp3');
INSERT INTO `song` VALUES (119, 47, '周杰伦-晴天', '叶惠美', '2020-12-28 13:58:32', '2020-12-28 13:58:32', '/img/songPic/1609163956281晴天.jfif', '[00:00.18]周杰伦 - 晴天\r\n[00:01.52]词：周杰伦\r\n[00:02.64]曲：周杰伦\r\n[00:29.39]故事的小黄花\r\n[00:32.64]从出生那年就飘着\r\n[00:36.19]童年的荡秋千\r\n[00:39.93]随记忆一直晃到现在\r\n[00:42.99]Re So So Si Do Si La\r\n[00:46.11]So La Si Si Si Si La Si La So\r\n[00:49.79]吹着前奏望着天空\r\n[00:53.35]我想起花瓣试着掉落\r\n[00:56.78]为你翘课的那一天\r\n[00:59.03]花落的那一天\r\n[01:00.65]教室的那一间\r\n[01:02.21]我怎么看不见\r\n[01:04.02]消失的下雨天\r\n[01:05.71]我好想再淋一遍\r\n[01:09.95]没想到失去的勇气我还留着\r\n[01:16.13]好想再问一遍\r\n[01:17.87]你会等待还是离开\r\n[01:24.74]刮风这天我试过握着你手\r\n[01:30.42]但偏偏雨渐渐大到我看你不见\r\n[01:38.84]还要多久我才能在你身边\r\n[01:45.40]等到放晴的那天也许我会比较好一点\r\n[01:52.76]从前从前有个人爱你很久\r\n[01:58.50]但偏偏风渐渐把距离吹得好远\r\n[02:06.74]好不容易又能再多爱一天\r\n[02:13.35]但故事的最后你好像还是说了拜拜\r\n[02:35.00]为你翘课的那一天\r\n[02:36.75]花落的那一天\r\n[02:38.63]教室的那一间\r\n[02:40.31]我怎么看不见\r\n[02:42.05]消失的下雨天\r\n[02:43.74]我好想再淋一遍\r\n[02:47.98]没想到失去的勇气我还留着\r\n[02:54.53]好想再问一遍\r\n[02:55.97]你会等待还是离开\r\n[03:02.77]刮风这天我试过握着你手\r\n[03:08.45]但偏偏雨渐渐大到我看你不见\r\n[03:16.87]还要多久我才能在你身边\r\n[03:23.49]等到放晴的那天也许我会比较好一点\r\n[03:30.91]从前从前有个人爱你很久\r\n[03:37.03]偏偏风渐渐把距离吹得好远\r\n[03:44.77]好不容易又能再多爱一天\r\n[03:51.07]但故事的最后你好像还是说了拜拜\r\n[03:59.62]刮风这天我试过握着你手\r\n[04:02.05]但偏偏雨渐渐大到我看你不见\r\n[04:05.44]还要多久我才能够在你身边\r\n[04:08.99]等到放晴那天也许我会比较好一点\r\n[04:12.86]从前从前有个人爱你很久\r\n[04:15.86]但偏偏雨渐渐把距离吹得好远\r\n[04:19.29]好不容易又能再多爱一天\r\n[04:22.79]但故事的最后你好像还是说了拜', '/song/周杰伦-晴天.mp3');
INSERT INTO `song` VALUES (120, 48, '陈奕迅-红玫瑰', '认了吧', '2020-12-28 15:31:34', '2020-12-28 15:31:34', '/img/songPic/1609169541359红玫瑰.jfif', '[00:00.84]陈奕迅 - 红玫瑰\r\n[00:03.93]词：李焯雄\r\n[00:06.43]曲：梁翘柏\r\n[00:08.61]编曲：梁翘柏\r\n[00:16.28]梦里梦到醒不来的梦\r\n[00:18.65]红线里被软禁的红\r\n[00:23.53]所有刺激剩下疲乏的痛\r\n[00:26.77]再无动于衷\r\n[00:30.01]从背后抱你的时候\r\n[00:33.94]期待的却是他的面容\r\n[00:37.63]说来实在嘲讽 我不太懂\r\n[00:41.18]偏渴望你懂\r\n[00:44.74]是否幸福轻得太沉重\r\n[00:48.42]过度使用不痒不痛\r\n[00:52.16]烂熟透红空洞了的瞳孔\r\n[00:56.35]终于掏空 终于有始无终\r\n[00:59.72]得不到的永远在骚动\r\n[01:03.40]被偏爱的 都有恃无恐\r\n[01:06.95]玫瑰的红 容易受伤的梦\r\n[01:11.32]握在手中却流失于指缝\r\n[01:14.63]又落空\r\n[01:31.15]红是朱砂痣烙印心口\r\n[01:33.77]红是蚊子血般平庸\r\n[01:38.45]时间美化那仅有的悸动\r\n[01:41.75]也磨平激动\r\n[01:45.12]从背后抱你的时候\r\n[01:48.93]期待的却是他的面容\r\n[01:52.67]说来实在嘲讽\r\n[01:55.04]我不太懂 偏渴望你懂\r\n[01:59.72]是否幸福轻得太沉重\r\n[02:03.34]过度使用 不痒不痛\r\n[02:07.15]烂熟透红空洞了的瞳孔\r\n[02:11.39]终于掏空 终于有始无终\r\n[02:14.70]得不到的永远在骚动\r\n[02:18.44]被偏爱的都有恃无恐\r\n[02:22.06]玫瑰的红 容易受伤的梦\r\n[02:26.30]握在手中却流失于指缝\r\n[02:29.74]又落空\r\n[02:44.78]是否说爱都太过沉重\r\n[02:48.39]过度使用不痒不痛\r\n[02:52.20]烧得火红 蛇行缠绕心中\r\n[02:56.35]终于冷冻终于有始无终\r\n[02:59.72]得不到的永远在骚动\r\n[03:03.28]被偏爱的都有恃无恐\r\n[03:06.96]玫瑰的红 容易受伤的梦\r\n[03:11.26]握在手中却流失于指缝\r\n[03:15.01]得不到的永远在骚动\r\n[03:18.44]被偏爱的 都有恃无恐\r\n[03:21.81]玫瑰的红 伤口绽放的梦\r\n[03:26.24]握在手中却流失于指缝\r\n[03:29.61]再落空', '/song/陈奕迅-红玫瑰.mp3');
INSERT INTO `song` VALUES (121, 53, 'Aimer-茜さす (夕晖)', '茜さす/everlasting snow', '2020-12-28 16:10:26', '2020-12-28 16:10:26', '/img/songPic/1609171884978夕晖.jfif', '[00:00:00]暂无歌词', '/song/Aimer (エメ) - 茜さす (夕晖).mp3');

-- ----------------------------
-- Table structure for song_list
-- ----------------------------
DROP TABLE IF EXISTS `song_list`;
CREATE TABLE `song_list`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `introduction` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `style` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '无',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of song_list
-- ----------------------------
INSERT INTO `song_list` VALUES (84, '只要有爱，我们就从来不缺少面对明天的勇气', '/img/songListPic/1609172022146109951165566246573.jpg', '在今年这个特殊的时间里，面对各种各样的变化、转折和失去，我们能够仰仗和依靠的，依然只有爱。 遇见你，我才发现，我可能并不缺少冒险的勇气，只是缺少一个冒险的理由。 当明天到来，你还会记得最纯粹的感动和爱吗?如果有幸等到你，这个冬天一起去看极光吧！', '华语');
INSERT INTO `song_list` VALUES (85, '一秒沉醉 • 欧美迷情仙女嗓', '/img/songListPic/1609172100878109951165525778915.jpg', '听这些仙女嗓有没有想和她们谈恋爱的冲动鸭～', '欧美');
INSERT INTO `song_list` VALUES (86, '「纯音乐」温馨美好，涤荡心扉的旋律', '/img/songListPic/1609172178225109951165374317125.jpg', '穿越古典和流行，带你感受旋律与心灵的共鸣，收录那些最触动我们心扉的唯美钢琴曲', '轻音乐');
INSERT INTO `song_list` VALUES (87, 'BODYPUMP 115 - Q4/2020', '/img/songListPic/1609172297660109951165392751269.jpg', 'BODYPUMP 115 - Q4/2020', '日韩');
INSERT INTO `song_list` VALUES (88, '林涧丨白噪音与乐音的融合', '/img/songListPic/1609172408164109951165406899770.jpg', '我是你，你是我。我们是孤独的，但又不孤独。我们被困在时间里，但又是无限的。我们是凡人肉身，也是日月星辰。', '纯音乐');
INSERT INTO `song_list` VALUES (89, '『偏愛亦是救赎』', '/img/songListPic/1609172467211109951165452977096.jpg', '月亮无法坠入爱河 星星得不到爱火', '粤语');
INSERT INTO `song_list` VALUES (90, '听着歌迎着光走，去探索五彩斑斓的世界', '/img/songListPic/1609173184888109951165496880365.jpg', '要大笑，要做梦，要与众不同，因为人生就是一场孤独的冒险；其实到了最后，我们都是在寻找同类，就像溪流汇入大海，光束拥抱彩虹一样，孤独终究不再孤独。', '纯音乐');
INSERT INTO `song_list` VALUES (91, '好好去爱这个世界 因为你值得', '/img/songListPic/1609172594524109951165177312849.jpg', '努力工作 努力吃饭  努力睡觉 努力玩乐  努力生活 努力过好这一生  时间一直往前走 留下来许多美好与遗憾 这就是成长吧  其实这个世界偶尔也擅长创造惊喜 我们会遇到温柔的事物 温柔的人 对这个世界也不必过于紧张 毕竟一切都终有归途 就像行星属于宇宙 希望我也能找到你  那些看似不起波澜的日复一日 会在某天让你看到坚持的意义 祝大家的坚持都会有好的结果', '纯音乐');
INSERT INTO `song_list` VALUES (92, '考研倒计时：你要悄悄拔尖，然后惊艳所有人', '/img/songListPic/1609173071503109951165499741988.jpg', '你背单词时 阿拉斯加的鳕鱼正跃出水面 你算数学时 太平洋彼岸的海鸥振翅掠过城市上空 你晚自习时 极图中的夜空散漫了五彩斑斓 但是少年你别着急 在你为自己未来踏踏实实地努力时 那些你感觉从来不会看到的景色 那些你觉得终身不会遇到的人 正一步步向你走来  距2021考研还剩不到一个月 愿你悄悄拔尖 惊艳所有人 愿爱你所爱 求你所求 得你所得 愿不负光阴 不负自己 不负被爱', '纯音乐');
INSERT INTO `song_list` VALUES (93, '做全世界的大人，只做你一个人的小朋友', '/img/songListPic/1609173137669109951165474220628.jpg', '成年人的世界没有容易二字。 很想做个小孩。不管前路多难，有你伴我前行。 也愿每个善良的你都能在爱的包围中勇往直前。 我可以做全世界的大人，却只做你一个人的小朋友。', '华语');

SET FOREIGN_KEY_CHECKS = 1;
