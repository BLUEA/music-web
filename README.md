# music-web

#### 介绍
JavaWeb课程设计，一个音乐网站，管理员可以在manage页面管理歌曲和歌单、歌手。
歌曲的文件和部分图片的文件太大没有上传

#### 软件架构
网站用的是前后端分离的框架，client和manage是两个Vue项目，server是一个SpringBoot项目。


#### 安装教程

音乐文件：
链接：https://pan.baidu.com/s/1syHr3hXKp0XM47129lif6Q 
提取码：kcsz 
复制这段内容后打开百度网盘手机App，操作更方便哦

下载的压缩包解压，将三个文件夹放在src同目录下；

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
